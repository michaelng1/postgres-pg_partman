FROM --platform=linux/amd64 postgres:15.4-bookworm

# install pg_partman & pg_cron
RUN apt-get update && apt-get install -y postgresql-15-partman

CMD ["postgres"]